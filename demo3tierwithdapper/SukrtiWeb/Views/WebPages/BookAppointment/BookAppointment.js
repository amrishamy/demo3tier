﻿'use strict'
app.controller('BookAppointment', ['$scope', '$window', 'CommonBase', 'notifications', '$http', 'uiCalendarConfig', '$filter',
    function ($scope, $window, CommonBase, notifications, $http, uiCalendarConfig, $filter) {

        $scope.SelectedEvent = null;
        $scope.ShowAvailable = true;
        $scope.ShowBooked = false;
        var isFirstTime = true;
        var registrationinfo = $window.JSON.parse($window.localStorage.getItem('RegistrationInfo'));
        var user_id = registrationinfo.Basic.UserId;


        FetchAllsAvailabilities();

        $scope.BookAppointmentObject = {

            UserID: user_id, OrderID: 0, AvailabilityID: 0, Price: 0, AppointementDate: '', AppointementFromTime: '', AppointementToTime: '',
        };

        $scope.ModelPopupTitle = "";

        //Load events from server

        function FetchAllsAvailabilities() {
            $scope.events = [];
            $scope.eventSources = [$scope.events];

            $http.get('/FetchAllsAvailability', {
                cache: false,
                params: {}
            }).then(function (data) {
                 //console.log(data.data.Basic);
                $scope.events.slice(0, $scope.events.length);

                if (data.data.Basic && data.data.Basic.length > 0) {
                    angular.forEach(data.data.Basic, function (outerValue) {
                       
                        angular.forEach(outerValue.AvailabilityDetailsList, function (value) {

                            var date = new Date(outerValue.AvailabilityDate);
                            var d = date.getDate();
                            var m = date.getMonth();
                            var y = date.getFullYear();

                            var fromHour = parseInt(value.FromTime);
                            var fromMinutes = parseInt(value.FromTime.substr(3));
                            var toHour = parseInt(value.ToTime);
                            var toMinutes = parseInt(value.ToTime.substr(3));

                            $scope.events.push({
                                AvailabilityID: outerValue.AvailabilityID,
                                title: outerValue.ContactPerson,
                                AppointementDate: outerValue.AvailabilityDate,
                                price: value.Price,
                                start: new Date(y, m, d, fromHour, fromMinutes),
                                end: new Date(y, m, d, toHour, toMinutes),
                                displaytime: value.DisplayTime,
                                isbooked: value.IsBooked,
                                // allDay: 0,
                                stick: true
                            });
                        });

                    });

                    
                    setTimeout(function () {
                        $scope.eventSources = [$scope.events];
                        $('#calendar').fullCalendar('removeEvents')
                        $('#calendar').fullCalendar('addEventSource', $scope.events)
                        $scope.$apply();
                    }, 300)

                }

            });
        }







        //--------------------------------------------------Save Appointment function----------------------------------------------------------------///

        $scope.SaveOrdersAndAppointementDetails = function () {
            if (confirm("Are you sure want to book this appointment?")) {
                CommonBase.Post('/SaveOrdersAndAppointement/', $scope.BookAppointmentObject).then(function (data) {
                    console.log(data);
                    if (data.Status > 0) {
                        $('#myModal').modal('toggle');
                        $('#myModal').modal('hide');
                        setNotification(notifications, 'success', 'Success Header', data.Message);
                        alert(data.Message);
                        FetchAllsAvailabilities();
                    }
                    else {
                        setNotification(notifications, 'danger', 'Error Header', data.Message);
                    }
                });


            }
        }



        //configure calendar
        $scope.uiConfig = {
            calendar: {
                height: 450,
                editable: true,
                displayEventTime: true,
                header: {
                    left: 'month basicWeek basicDay agendaWeek agendaDay',
                    center: 'title',
                    right: 'today prev,next'
                },
                eventClick: function (event) {

                    $scope.SelectedEvent = event;
                    
                    //$("#eventContent").dialog({ modal: true, title: event.title, });
                    $('#myModal').modal('toggle');
                    $('#myModal').modal('show');
                   
                    $scope.ModelPopupTitle = event.title;

                },
                eventRender: function (event, element) {
                    element.attr('href', 'javascript:void(0);');
                    element.click(function () {

                        //$("#startTime").html(moment(event.start).format('MMM Do h:mm A'));
                        //$("#endTime").html(moment(event.end).format('MMM Do h:mm A'));
                        //$("#eventInfo").html(event.description);
                        //$("#eventPrice").html(event.price);
                        //$("#eventdate").html(moment(event.AppointementDate).format('MMMM Do YYYY'));

                        $scope.BookAppointmentObject.AppointementFromTime = (moment(event.start).format('h:mm A')); //event.start;
                        $scope.BookAppointmentObject.AppointementToTime = (moment(event.end).format('h:mm A'));// event.end;
                        $scope.BookAppointmentObject.Price = event.price;
                        $scope.BookAppointmentObject.isBooked = event.isbooked;
                        $scope.BookAppointmentObject.AvailabilityID = event.AvailabilityID;
                        $scope.BookAppointmentObject.AppointementDate = $filter('date')(event.AppointementDate, "MM-dd-yyyy");
                        if (event.isbooked == 1) {
                            $scope.ShowAvailable = false;
                            $scope.ShowBooked = true;
                        }
                        else {
                            $scope.ShowAvailable = true;
                            $scope.ShowBooked = false;
                        }
                        

                    });
                    if (event.isbooked == 1) {
                        element.css({
                            'background-color': '#f80000',
                            'border-color': '#333333'
                        });
                    }
                    else {
                        element.css({
                            'background-color': '#228B22',
                            'border-color': '#228B22'
                        });
                    }
                },
                //dayRender: function (cell,event) {
                   
                //    console.log(event.isbooked);
                //    if ($scope.BookAppointmentObject.isBooked == 1) {
                //        alert();
                //        cell.css("background-color", "red");
                //    }
                //},
                //dayRender: function (date, cell,event) {
                   
                //    event.css("background-color", "red");
                    
                //},
                eventAfterAllRender: function (element) {
                    if ($scope.events.length > 0 && isFirstTime) {
                        //Focus first event
                        uiCalendarConfig.calendars.myCalendar.fullCalendar('gotoDate', $scope.events[0].start);
                        isFirstTime = false;
                       
                    }
                    
                }

            }

        };

        //------------------for alert messages...............................
        $scope.notify = notifications;
        $scope.closeAlert = function (item) {
            notifications.pop(item);
        }
    }])

