﻿using BusinessLogic.Admin.Login;
using BusinessObjects.Admin.Users;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace SukrtiWeb.Authorization
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated(); // 
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            LoginManager _objLoginManager = new LoginManager();
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            LoginReturnViewModel _reportView = new LoginReturnViewModel();

            _reportView = _objLoginManager.FetchLoginDetails(context.UserName, context.Password);
            if (_reportView.Basic != null && (_reportView.Basic.UserId > 0 || _reportView.Basic.UserId == -102))
            {
                identity.AddClaim(new Claim(ClaimTypes.Name, _reportView.Basic.Email == null ? "" : _reportView.Basic.Email));
                identity.AddClaim(new Claim("UserId", _reportView.Basic.UserId.ToString()));

                Dictionary<string, string> properties = new Dictionary<string, string>();
                properties.Add("LoginObject", JsonConvert.SerializeObject(_reportView.Basic));

                AuthenticationProperties properties1 = new AuthenticationProperties(properties);
                AuthenticationTicket ticket = new AuthenticationTicket(identity, properties1);
                context.Validated(ticket);

                context.Validated(identity);
                // JToken.FromObject(context);
            }
            else
            {
                context.SetError("invalid_grant", _reportView.Message);
                return;
            }
        }
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}