﻿'use strict'
app.factory('CommonBase', function ($http, $q, $window) {

    return {
        //For Get Data
        Get: function (ControllerCall) {
            var deferred = $q.defer();
            $http.get( ControllerCall)
            .success(function (data, status) {

                deferred.resolve(data);
            })
            .error(function (data, status) {
                deferred.reject(data);
            });

            return deferred.promise
        },

        //For POST Data
        Post: function (ControllerCall, Obj) {
            var deferred = $q.defer();
            $http.post( ControllerCall, Obj)
                .success(function (data, status) {
                deferred.resolve(data);
            })
            .error(function (data, status) {
                deferred.reject(data);
            });

            return deferred.promise
        },

        //For POST Without Obj Data
        PostWithoutObj: function (ControllerCall) {
            var deferred = $q.defer();
            $http.post( ControllerCall)
            .success(function (data, status) {
                deferred.resolve(data);
            })
            .error(function (data, status) {
                deferred.reject(data);
            });
            return deferred.promise
        },

        //For POST With Two Object Data
        PostWithTwoObj: function (ControllerCall, Obj1, Obj2) {
            var deferred = $q.defer();
            $http.post( ControllerCall, Obj1, Obj2)
             .success(function (data, status) {
                 deferred.resolve(data);
             })
             .error(function (data, status) {
                 deferred.reject(data);
             });
                    return deferred.promise
        }

    };
});


