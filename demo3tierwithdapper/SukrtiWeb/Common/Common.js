﻿app.directive('digitsOnly', function () {
    return {
        require: 'ngModel', link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) {
                    return '';
                }
                var transformedInput = inputValue.replace(/[^0-9]/g, '')
                var arr = transformedInput.split('');
                count = 0;
                transformedInput = arr.toString().replace(/,/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

app.service('notifications', ['$rootScope', function ($rootScope) {
    var queue = [];
    return {
        queue: queue,
        add: function (item) {
            var index = -1;
            //check alert with same body not active in dom
            for (var i = 0; i < this.queue.length; i++) {
                if (queue[i].body == item.body) {
                    index = i;
                    break;
                }
            }
            if (index != -1)
                return;
            queue.push(item);
            setTimeout(function () {
                $('.alerts .alert').eq(0).remove();
                queue.shift();
            }, 2000);
        },
        pop: function (item) {
            var index = -1;
            //to find alert from queue of active alerts in dom
            for (var i = 0; i < this.queue.length; i++) {
                if (queue[i].body == item) {
                    index = i;
                    break;
                }
            }
            if (index != -1)
                queue.splice(index, 1);
            return this.queue;
        }
    };



}]);

function setNotification(notifications, type, title, body) {
    notifications.add({
        type: type,
        title: title,
        body: body
    });
};


app.factory('FileUploadService', ['CommonBase', '$q', function (CommonBase, $q) {
    var fac = {};
    fac.UploadFile = function (file) {

        var formData = new FormData();
        formData.append("file", file);
        var defer = $q.defer();
        CommonBase.PostWithTwoObj('/Upload_File/', formData, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(function (data) {

            if (data != null) {
                //alert("file uploaded successfully!");
                defer.resolve(data);
            }
            else {
                alert("file upload failed!");
                defer.reject("File Upload Failed!");
            }
        });
        return defer.promise;
    }
    return fac;

}]);

app.factory('Cookie', ['CommonBase', '$q', function (CommonBase, $q) {
    var cookie = {};
    cookie.GetCookie = function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
    }
    cookie.SetCookie = function (name, value, hours) {
        var expires = "";
        if (hours) {
            var date = new Date();
            date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }
    cookie.DeleteCookie = function (name, value, days) {
        var expires = "";
        var date = new Date();
        document.cookie = name + '=;expires=' + date.setTime(date.getTime() + (days * 1 * 1000)) + "; path=/";
    }
    return cookie;
}]);

app.directive('ngConfirmClick', [
  function () {
      return {
          priority: -1,
          restrict: 'A',
          link: function (scope, element, attrs) {
              element.bind('click', function (e) {
                  var message = attrs.ngConfirmClick;
                  if (message && !confirm(message)) {
                      e.stopImmediatePropagation();
                      e.preventDefault();
                  }
              });
          }
      }
  }
]);

app.directive('decimalWithdot', function () {
    return {
        restrict: 'A',
       
        link: function (scope, elm, attrs, ctrl) {
            elm.on('keydown', function (event) {
                var $input = $(this);
                var value = $input.val();
                value = value.replace(/[^0-9\.]/g, '')
                var findsDot = new RegExp(/\./g)
                var containsDot = value.match(findsDot)
                if (containsDot != null && ([46, 110, 190].indexOf(event.which) > -1)) {
                    event.preventDefault();
                    return false;
                }
                $input.val(value);
                if (event.which == 64 || event.which == 16) {
                    // numbers  
                    return false;
                } if ([8, 13, 27, 37, 38, 39, 40, 110].indexOf(event.which) > -1) {
                    // backspace, enter, escape, arrows  
                    return true;
                } else if (event.which >= 48 && event.which <= 57) {
                    // numbers  
                    return true;
                } else if (event.which >= 96 && event.which <= 105) {
                    // numpad number  
                    return true;
                } else if ([46, 110, 190].indexOf(event.which) > -1) {
                    // dot and numpad dot  
                    return true;
                }
                else if(event.which==9)
                {
                    elm.next().focus();
                }
                else {
                    event.preventDefault();
                    return false;
                }
            });
          
        }
    }
});


app.directive('datePicker', function () {
    return {
        restrict: 'EA',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            var datepicker;
            var optionsObj = {
                dateFormat: 'mm/dd/yy',
                onSelect: function (dateText, inst) {
                    scope.$apply(function () {
                        ngModel.$setViewValue(dateText);
                        blur();
                    });
                },
                onClose: function (dateText, inst) {
                    $(element).blur();
                }
            };
            datepicker = $(element).datepicker(optionsObj);
            scope.$watch(attrs.ngModel, function (value) {
                //datepicker.datepicker('setDate', new Date(ngModel.$viewValue));
                datepicker.datepicker('refresh');
            });
        }
    };
});

app.directive('modalpopup', function () {
    return {
        template: '<div class="modal fade" ng-transclude>' +

          '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;

            scope.$watch(attrs.visible, function (value) {
                if (value == true)
                    $(element).modal('show');
                else
                    $(element).modal('hide');
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});

app.directive("limitTo", [function () {
    return {
        restrict: "A",
        link: function (scope, elem, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).on("keypress", function (e) {
                if (this.value.length >= limit) e.preventDefault();
            });
        }
    }
}]);

app.filter('startFrom', function () {
    return function (input, start) {
        if (!input || !input.length) {
            return;
        }
        start = +start;
        return input.slice(start);
    };
});
app.factory('DownloadFilefact', function ($window, $location) {
    return {
        DownloadFile: function (dataUrl, data) {
            var userAgent = $window.navigator.userAgent;
            if ((userAgent.indexOf("Chrome")) != -1) {
                var link = document.createElement('a');
                angular.element(link)
                  .attr('href', dataUrl)
                  .attr('download', data)
                  .attr('target', '_blank')
                link.click();
            }
            else {
                var URL = $location.absUrl();
                URL = URL.substr(0, URL.indexOf('/M')) + dataUrl;

                window.location.assign(URL);
            }
        }
    }
});

app.directive('ngEnter', function () { //a directive to 'enter key press' in elements with the "ng-enter" attribute
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});

app.directive('usSpinner', ['$http', '$rootScope', function ($http, $rootScope) {
    return {
        link: function (scope, elm, attrs) {
            $rootScope.spinnerActive = false;

            scope.isLoading = function () {

                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function (loading) {
                $rootScope.spinnerActive = loading;
                if (loading) {
                    elm.removeClass('ng-hide');
                    document.getElementById('spinner_loading').style.display = 'block';
                } else {
                    elm.addClass('ng-hide');
                    document.getElementById('spinner_loading').style.display = 'none';
                }
            });
        }
    };

}]);

app.directive('datePickermindate', function () {
    return {
        restrict: 'EA',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            var datepicker;
            var optionsObj = {
                dateFormat: 'mm/dd/yy',
                minDate:0,
                onSelect: function (dateText, inst) {
                    scope.$apply(function () {
                        ngModel.$setViewValue(dateText);
                        blur();
                    });
                },
                onClose: function (dateText, inst) {
                    $(element).blur();
                }
            };
            datepicker = $(element).datepicker(optionsObj);
            scope.$watch(attrs.ngModel, function (value) {
                //datepicker.datepicker('setDate', new Date(ngModel.$viewValue));
                datepicker.datepicker('refresh');
            });
        }
    };
});

app.directive('validdecimalNumber', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }
                }
                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
app.factory('myService', function () {

    var savedData = 0;
    function set(data) {
        savedData = data;
       }
    function get() {
        return savedData;

    }

    return {
        set: set,
        get: get
    }
    
});

app.directive('allowNumbersAlphabetsHyphens', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^A-Za-z0-9-]/g, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('allowHyphensNumbersAlphabets', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^A-Za-z0-9-]/g, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
app.directive('allowNumbersAlphabets', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^A-Za-z0-9]/g, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
app.directive('numberscolonOnly', function () {
    return {
        require: 'ngModel', link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) {
                    return '';
                }
                var transformedInput = inputValue.replace(/[`b-ln-oq-zA-Z~!@#$%^&*_+\-\\=\[\]{};'"\\|,.<>\/?]/g, '');

                var arr = transformedInput.split('');
                var count = 0;
                transformedInput = arr.toString().replace(/,/g, '');

                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

app.service('CartService', function () {
    this.cartValue = 0;
    this.cartPrice = 0;
});
app.directive('starRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star">' +
            '\u2605' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '=',
            max: '='
        },
        link: function ($scope, elem, attrs) {
            var updateStars = function () {
                $scope.stars = [];
                for (var i = 0; i < $scope.max; i++) {
                    $scope.stars.push({
                        filled: i < $scope.ratingValue
                    });
                }
            };
            updateStars();
            $scope.$watch('ratingValue', function (oldVal, newVal) {
                if (newVal) {
                    updateStars();
                }
            });
        }
    }
});
app.directive('digitsOnly', function () {
    return {
        require: 'ngModel', link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) {
                    return '';
                }
                var transformedInput = inputValue.replace(/[^0-9]/g, '')
                var arr = transformedInput.split('');
                var count = 0;
                transformedInput = arr.toString().replace(/,/g, '');

                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});
app.directive('starsRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
            '\u2605' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '=',
            max: '=',
            onRatingSelected: '&'
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];

                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };

            scope.toggle = function (index) {
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    rating: index + 1
                });
                updateStars();
            };

            scope.$watch('ratingValue', function (oldVal, newVal) {
                if (newVal) {
                    updateStars();
                }
            });
        }
    }

});
app.service('MetaTagService', function () {
    this.description = "Shopping Cart";
    this.keywords = '';
    this.title = "Shopping Cart";
    
});

app.filter('unique', function () {

    // Take in the collection and which field
    //   should be unique
    // We assume an array of objects here
    // NOTE: We are skipping any object which
    //   contains a duplicated value for that
    //   particular key.  Make sure this is what
    //   you want!
    return function (arr, targetField) {

        var values = [],
            i,
            unique,
            l = arr.length,
            results = [],
            obj;

        // Iterate over all objects in the array
        // and collect all unique values
        for (i = 0; i < arr.length; i++) {

            obj = arr[i];

            // check for uniqueness
            unique = true;
            for (v = 0; v < values.length; v++) {
                if (obj[targetField] == values[v]) {
                    unique = false;
                }
            }

            // If this is indeed unique, add its
            //   value to our values and push
            //   it onto the returned array
            if (unique) {
                values.push(obj[targetField]);
                results.push(obj);
            }

        }
        return results;
    };
})