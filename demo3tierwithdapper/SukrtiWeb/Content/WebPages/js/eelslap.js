// vars
var $imgs = $('.eelimages'),
    baseImgName = "big-",
    folder1 = "images/image1/" + baseImgName,
    folder2 = "images/image2/" + baseImgName,
    folder3 = "images/image3/" + baseImgName;
    folder4 = "images/image3/" + baseImgName;
    folder5 = "images/image3/" + baseImgName;
    folder6 = "images/image3/" + baseImgName;
    folder7 = "images/image3/" + baseImgName;

var currentNumber;

function randomMovie() {
    currentNumber = $($imgs[0]).attr("src").split("/")[1].substring(5,6);
    
    var minNumber = 1, // min folder
        maxNumber = 7; // max folder
    
    var randomNumber = Math.floor(Math.random()*(maxNumber-minNumber+1)+minNumber);
    if (randomNumber == currentNumber) {
        if (currentNumber < maxNumber) {
            randomNumber++;
        } else {
            randomNumber--;
        }
    }
    
    var randomFolder = "images/image" + randomNumber + "/" + baseImgName;

    $imgs.each(function(index) {
        var imageNumber = index+1;
        $("#eelimage" + imageNumber).attr("src", randomFolder + imageNumber + "-min.jpg");
    });
    
};



 $(document).ready(function() {
    // event handler to trigger random movie function
    
    /* Lo hemos quitado 22 de Enero

    $(".vimeo-play-button").mouseenter(function() {
        randomMovie();
    });

    */


//    $(".movieSwitcher").hover(function() {
//        randomMovie();
//    });
});

(function(window, $, undefined) {
	var app = window.Eel || (window.Eel = {});
	var $window = $(window);

	var currentPosition = 0;
	var targetPosition = 0;
	var browserWidth = 0;

	var loadedImages = 0;

	var autorun = function() {

		$(".eelimages").one('load', function() {
			imageLoaded();
		}).each(function() {
			if(this.complete) $(this).load();
		});

		for (var number=0; number < 48; number++) {
		   $("#eelimage" + number).attr("src", "images/image1/big-" + number + "-min.jpg");
		}

		//startSlap();
	};

	var imageLoaded = function() {
		loadedImages++;

		if (loadedImages == 48) {
			$("#loader").animate({ opacity: 0 }, 500, "linear", function() {
				/* $("#loader").css("display","none"); */
			});
			setTimeout(function() {
				$("#allimages").css("display","block");
				$("#allimages").animate({ opacity: 1 }, 3000, "linear");
				/*
				if (isTouchDevice()) {
					setTimeout(function() {
						$("#introtext").css("display","block");
						$("#introtext").html("Drag your finger across the screen to slap!");
						$("#introtext").css("display","block");
						$("#introtext").animate({ opacity: 1 }, 1000, "linear");

						setTimeout(function() {
							$("#introtext").animate({ opacity: 0 }, 1000, "linear", function() {
								$("#introtext").css("display","none");
							});
						}, 3000);
					}, 1000);
				}
				*/
				startSlap();
			}, 500);
		}
	};

	/* ACELEROMETRO */

	init();
	  var count = 0;

	  function init() {
	    if (window.DeviceOrientationEvent) {
	      window.addEventListener('deviceorientation', function(eventData) {
	        var tiltFB = eventData.beta;
	        var tiltLR = eventData.gamma;

	        deviceOrientationHandler(tiltFB, tiltLR);
	      }, false);
	    }
	  }

	  function deviceOrientationHandler(tiltFB, tiltLR) {
	    moveBall(tiltFB, tiltLR);
	  }

	  function moveBall(tiltFB, tiltLR) {
//	  	targetPosition = -(tiltLR) * 20 - $('#eelcontainer').offset().left;
          
          targetPosition = ((45 + tiltLR) / 90) * browserWidth;
          
//          targetPosition = 1536 - Math.max(0, Math.min(1536, e.pageX - $('#eelcontainer').offset().left));
          
          
	  	$('.ball').css('left', 50 + (tiltLR) + '%');
          
          $('#tiltLR')[0].innerHTML = tiltLR;
          $('#targetPosition')[0].innerHTML = targetPosition;
 
	  	
//	  	if (Math.round(tiltLR) == 0) {
//            randomMovie();
//		}

	  }
    
    //listen to shake event
    var shakeEvent = new Shake({threshold: 15});
    shakeEvent.start();
    window.addEventListener('shake', function(){
        randomMovie();
    }, false);

	  /* FIN ACELEROMETRO */

	var startSlap = function() {
		browserWidth = $(window).width();

		setInterval(function() {
			currentPosition += (targetPosition - currentPosition) / 1;
			var currentSlap = currentPosition / browserWidth * 47;
			currentSlap = Math.min(48, Math.max(0,currentSlap));
			var pos = Math.round(currentSlap) * -1536;

			$("#allimages").css("left", pos);
            
                $('#currentSlap')[0].innerHTML = currentSlap;
            
		}, 1);

		$("body").bind('mousemove', function(e) {
            targetPosition = e.pageX;
            
                $('#targetPosition')[0].innerHTML = targetPosition;
                $('#epageX')[0].innerHTML = e.pageX;
		});

		/* 
		$("body").bind('touchmove', function(e) {
			e.preventDefault();
			var touch = event.targetTouches[event.targetTouches.length-1];
			$("#bugger").html("TOUCH: " + touch.pageX + ", " + event.targetTouches.length);
			targetPosition = browserWidth - touch.pageX;
		});
		*/

		$(window).resize(function() {
			browserWidth = $(window).width();
		});
	};

	var isTouchDevice = function() {
		var el = document.createElement('div');
		el.setAttribute('ongesturestart', 'return;');
		return typeof el.ongesturestart === "function";
	};

	// On DOM ready
	$(autorun);
    

})(this, jQuery);
