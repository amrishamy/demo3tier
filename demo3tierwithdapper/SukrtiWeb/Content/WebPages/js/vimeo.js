'use strict';

new Vue({
  el: '#app',
  data: {
    duration: '01:15',
    video_url: 'https://player.vimeo.com/video/229253340',
    show: false,
    player: {},
    thumbnail: '',
    title: 'Small Films'
  },
  name: 'VideoHero',
  methods: {
    play: function play() {
      this.lockBody();
      this.player.play();
      this.show = true;
    },
    stop: function stop() {
      this.unload();
    },
    finish: function finish() {
      this.unload();
    },
    error: function error() {
      this.unload();
    },
    unload: function unload() {
      this.unlockBody();
      this.player.unload();
      this.show = false;
    },
    lockBody: function lockBody() {
      window.addEventListener('keydown', this.keydown);
      document.body.style.overflow = 'hidden';
      document.body.style.position = 'fixed';
    },
    unlockBody: function unlockBody() {
      window.removeEventListener('keydown', this.keydown);
      document.body.style.overflow = 'inherit';
      document.body.style.position = 'static';
    },
    keydown: function keydown(e) {
      if (this.show && e.keyCode == 27) {
        this.stop();
      }
    }
  },
  mounted: function mounted() {
    // add esc exit
    this.player = new Vimeo.Player(this.$refs.iframe);
    this.player.on('error', this.error);
    this.player.on('ended', this.finish);
  },
  destroyed: function destroyed() {
    this.player.off('error', this.error);
    this.player.off('ended', this.finish);
    this.player = null;
  }
});