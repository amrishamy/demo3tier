﻿using BusinessLogic.WebPages.BookAppointment;
using BusinessObjects.WebPages.BookAppointment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace SukrtiWeb.Controllers.WebPages.BookAppointment
{
    public class BookAppointmentController : ApiController
    {
       BookAppointmentManager _objBookAppointmentManager = new BookAppointmentManager();

        [HttpPost]
        [Route("SaveOrdersAndAppointement/")]
        public IHttpActionResult SaveOrdersAndAppointement(BookAppointmentObject _obj)
        {
            var _return_view_model = _objBookAppointmentManager.SaveOrdersAndAppointement(_obj);
            var json = JToken.FromObject(_return_view_model);
            return Ok(json);
        }
    }
}
