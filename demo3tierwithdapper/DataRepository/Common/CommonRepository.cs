﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataRepository.Common
{
    public class CommonRepository
    {
        public static DynamicParameters GetLogParameters()
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Stat", 0, DbType.Int32, ParameterDirection.Output);
            parameter.Add("@Message", "", DbType.String, ParameterDirection.Output);
            return parameter;

        }

        public static SqlConnection GetConnectionString()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        }
    }
}
