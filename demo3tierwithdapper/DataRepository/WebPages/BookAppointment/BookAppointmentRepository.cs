﻿using BusinessObjects.WebPages.BookAppointment;
using Dapper;
using DataRepository.Common;
using SqlDapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataRepository.WebPages.BookAppointment
{
    public class BookAppointmentRepository : DapperRepository
    {
        public BookAppointmentRepository(IDatabasecontext dbcontext)
            : base(dbcontext)
        { }

        public ResultObject SaveOrdersAndAppointement(BookAppointmentObject _obj)
        {
            ResultObject _resultObject = new ResultObject();
            try
            {
                DynamicParameters _parameters = CommonRepository.GetLogParameters();
                _parameters.Add("@OrderID", _obj.OrderID);
                _parameters.Add("@UserID", _obj.UserID);
                _parameters.Add("@AvailabilityID", _obj.AvailabilityID);
                _parameters.Add("@AppointementDate", _obj.AppointementDate);
                _parameters.Add("@AppointementFromTime", _obj.AppointementFromTime);
                _parameters.Add("@AppointementToTime", _obj.AppointementToTime);
                
                var result = databaseContext.Query<int>("SaveOrdersAndAppointement", _parameters, null, CommandType.StoredProcedure);
                if (result != null)
                {
                    _resultObject.Status = _parameters.Get<int>("@Stat");
                    _resultObject.Message = _parameters.Get<string>("@Message");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _resultObject;
        }
    }
}
