﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects.WebPages.BookAppointment
{
    public class BookAppointmentObject
    {
        public int OrderID { get; set; }
        public int UserID { get; set; }
        public int AvailabilityID { get; set; }
        public DateTime AppointementDate { get; set; }
        public string AppointementFromTime { get; set; }
        public string AppointementToTime { get; set; }
    }
    public class ResultObject
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
