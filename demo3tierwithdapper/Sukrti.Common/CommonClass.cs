﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.ComponentModel;

namespace Sukrti.Common
{
    public class CommonClass
    {
        public static DataTable GetDataTableFromExcel(String ExcelFile, String SheetName, int HeaderRowNumber)
        {
            try
            {
                // Create new DataSet to hold information from the worksheet.
                DataTable objDatatable = new DataTable();

                // Create connection string variable. Modify the "Data Source"
                // parameter as appropriate for your environment.

                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + ExcelFile + ";" + "Extended Properties=\"Excel 8.0;HDR=No;IMEX=1;\"";
                if (sConnectionString.Contains(".xlsx"))
                {
                    sConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ExcelFile + ";Extended Properties=\"Excel 12.0;HDR=No;IMEX=1\";";
                }
                // Create connection object by using the preceding connection string.
                OleDbConnection objConn = new OleDbConnection(sConnectionString);

                // Open connection with the database.
                objConn.Open();

                // The code to follow uses a SQL SELECT command to display the data from the worksheet.
                try
                {
                    // Create new OleDbCommand to return data from worksheet.
                    OleDbCommand objCmdSelect = new OleDbCommand(String.Format("SELECT * FROM [{0}$]", SheetName), objConn);

                    using (OleDbDataReader dr = objCmdSelect.ExecuteReader())
                    {

                        if (dr.HasRows)
                        {
                            objDatatable.Load(dr);
                        }
                        dr.Close();
                        dr.Dispose();
                    }

                    // Clean up objects.
                    objConn.Close();
                    objCmdSelect.Dispose();
                    string colnumber = "";
                    if (objDatatable.Rows.Count >= 1)
                    {
                        if (HeaderRowNumber > 1)
                        {
                            int i = HeaderRowNumber - 2;
                            for (var j = 0; j < HeaderRowNumber - 1; j++)
                            {
                                objDatatable.Rows[i].Delete();
                                i--;
                            }
                            objDatatable.AcceptChanges();
                        }
                        foreach (DataColumn dc in objDatatable.Columns)
                        {
                            if (objDatatable.Rows[0][dc.ColumnName].ToString().Trim().Length > 0)
                            { dc.ColumnName = objDatatable.Rows[0][dc.ColumnName].ToString(); }
                            else { colnumber += "," + dc.ColumnName; }
                        }
                        objDatatable.AcceptChanges();
                        objDatatable.Rows[0].Delete();
                        objDatatable.AcceptChanges();
                    }
                    return objDatatable;
                }
                catch (Exception ex)
                {
                    objConn.Close();
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public static DataTable Reade_2007_Excelfile(String ExcelFilePath)
        {
            DataTable dtExcelSchema;
            DataSet ds = new DataSet();
            //String strExcelConn = "Provider=Microsoft.ACE.OLEDB.12.0;"+ "Data Source=" + ExcelFilePath + ";"  + "Extended Properties='Excel 8.0;HDR=No'";
            String strExcelConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ExcelFilePath + ";Extended Properties=Excel 12.0 Xml;";
            OleDbConnection connExcel = new OleDbConnection(strExcelConn);
            OleDbCommand cmdExcel = new OleDbCommand();
            try
            {
                cmdExcel.Connection = connExcel;

                //Check if the Sheet Exists
                connExcel.Open();

                //Get the Schema of the WorkBook
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                connExcel.Close();

                //Read Data from Sheet1
                connExcel.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();

                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";

                //Range Query
                //cmdExcel.CommandText = "SELECT * From [" + SheetName + "A3:B5]";

                da.SelectCommand = cmdExcel;
                da.Fill(ds);
                connExcel.Close();

            }
            catch (Exception ex)
            {
                string str = ex.ToString();
                connExcel.Close();
                dtExcelSchema = null;
            }
            finally
            {
                cmdExcel.Dispose();
                connExcel.Dispose();
            }
            string colnumber = "";
            if (ds.Tables[0].Rows.Count >= 1)
            {
                foreach (DataColumn dc in ds.Tables[0].Columns)
                {
                    if (ds.Tables[0].Rows[0][dc.ColumnName].ToString().Trim().Length > 0)
                    { dc.ColumnName = ds.Tables[0].Rows[0][dc.ColumnName].ToString(); }
                    else { colnumber += "," + dc.ColumnName; }
                }
                ds.Tables[0].AcceptChanges();
                ds.Tables[0].Rows[0].Delete();
                ds.Tables[0].AcceptChanges();
            }
            foreach (string names in colnumber.Split(','))
            {
                if (names.Length > 0)
                    ds.Tables[0].Columns.Remove(names);
            }
            ds.Tables[0].AcceptChanges();
            return ds.Tables[0];
        }

        public static DateTime Get_full_DateTime(string date)
        {
            DateTime value = Convert.ToDateTime(date);
            return value;
        }

        public static string GeneratePassword()
        {
            string strPwdchar = "abcdefghijklmnopqrstuvwxyz0123456789#+@&$ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string strPwd = "";
            Random rnd = new Random();
            for (int i = 0; i <= 7; i++)
            {
                int iRandom = rnd.Next(0, strPwdchar.Length - 1);
                strPwd += strPwdchar.Substring(iRandom, 1);
            }
            return "S1" + strPwd + "@";
        }
        
        public static string GenerateRandomAlfaNumericString()
        {
            string strPwdchar = "abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHJKLMNOPQRSTUVWXYZ";
            string strPwd = "";
            Random rnd = new Random();
            for (int i = 0; i <= 15; i++)
            {
                int iRandom = rnd.Next(0, strPwdchar.Length - 1);
                strPwd += strPwdchar.Substring(iRandom, 1);
            }
            return strPwd;
        }

        public static string ResetPasswordHTML(string temporary_password)
        {
            string template = string.Empty;
            if (temporary_password != null)
            {
                template = "You can login with the following password.";
            }
            else
            {
                //template = "You forgot your password. No worries. It happens to the best of us. Reset your password with the link below.";
            }
            string Email = string.Empty;
            try
            {
                Email = "<div>"
                         + "<table cellpadding=\"0\" cellspacing=\"0\" style='width:560px' width='560'>"
                          + "<tr >"
                            + "<td width='100%' bgcolor='#ffffff'><table width='100%' cellpadding='0' cellspacing='0' border='0' align='center' class='m_-2262673184714871224max-width' style='max-width:620px'>"
                            + "   <tbody>"
                            + "     <tr>"
                            + "       <td style='border-top:6px solid #155b7f;padding:0 10px' >"
                            + "         <table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>"
                            + "           <tbody>"
                            + "             <tr>"
                            + "               <td align='center' style='padding:20px 0;border-bottom:5px solid #f0f0f0;line-height:0'><a href='#' style='display:inline-table;outline:none;line-height:0' target='_blank'><img src='https://d3d71ba2asa5oz.cloudfront.net/53000188/images/645071003-1.jpg' alt='' style='border:0' width='150' height='91' class='CToWUd'></a></td>"
                            + "             </tr>"
                            + "           </tbody>"
                            + "         </table></td>"
                            + "     </tr>"
                            + "   </tbody>"
                            + " </table>"
                            + "</td"
                            + "</tr>"
                         + "<tr>"
                         + "<td style=\"font-size: 13px;padding-top: 15px;\">"
                         + "The following list contains temporary password for newly created or modified user account."
                         + "</td>"
                         + "</tr>"
                         + "<tr>"
                         + "<td style=\"font-size: 13px;padding-top: 15px;\">Please note:</td>"
                         + "</tr>"
                         + "<tr>"
                         + "<td style=\"font-size: 13px;padding-left: 5px;padding-top: 15px;\">"
                         + "* When distributing IDs and passwords to individual users, be sure to do so in a safe and secure manner."
                         + "</td>"
                         + "</tr>"
                         + "<tr>"
                         + "<td style=\"font-size: 13px;padding-left: 5px;\">"
                         + "* Temporary passwords are valid for 30 days"
                         + "</td>"
                         + "</tr>"
                         + "<tr>"
                         + "<td style=\"padding-top: 20px;font-size: 13px;\">" + template + "</td>"
                         + "</tr>"
                         + "</br>"
                         + "<tr>"
                         + "<td style=\"font-size: 13px;\">"
                         + "Temporary Password: " + "<span style=\"color:#0561c7;\">" + temporary_password + "</span>" + ""
                         + "</td>"
                         + "</tr>"
                         + "<tr>"
                         + "<td style=\"font-size: 13px;\"><p>Once your end users have successfully signed in with their temporary passwords, they can create new passwords by following the instructions on the sign in page."
                         + "</p></td>"
                         + "</tr>"
                         + "<tr>"
                         + "<td style=\"font-size: 13px;\">"

                            + " <tr>"
                            + " <td width = '100%' bgcolor = '#ffffff' ><table width = '100%' cellpadding = '0' cellspacing = '0' border = '0' align = 'center' class='m_-2262673184714871224max-width' style='max-width:620px'>"
                            + " <tbody>"
                            + " <tr>"
                            + " <td bgcolor = '#155b7f' style='padding:17px 20px 10px 20px;text-align:left'><table width = '100%' cellpadding='0' cellspacing='0' border='0'>"
                            + " <tbody>"
                            + " <tr>"
                            + " <td style ='color:#ffffff;font:12px Arial' > We look forward to seeing you again soon.</td>"
                            + " </tr>"
                            + " <tr>"
                            + " <td style ='padding-top:5px;font:12px Arial;color:#ffffff' > Team Premiumshoe</td>"
                            + " </tr>"
                            + " <tr>"
                            + " <td style ='padding:20px 0 0px;font:12px/150% Arial;color:#ffffff' > Check out our<a href='#' style='color:#ffffff;text-decoration:underline;outline:none' target='_blank'> Easy Returns Policy</a> | "

                            + "  Browse top FAQs, please go to<a href='#' style='color:#ffffff;text-decoration:underline;font-weight:bold;outline:none' target='_blank'> HELP</a> | Support a cause via <a href = '#' style= 'color:#ffffff;text-decoration:underline;outline:none' target= '_blank' > SD Sunshine</a></td>"
                            + "  </tr>"
                            + "  </tbody>"
                            + "  </table></td>"
                            + "  </tr>"
                            + "  </tbody>"
                            + "  </table>"
                            + "  <table width = '100%' cellpadding= '0' cellspacing= '0' border= '0' align= 'center'>"
                            + "  <tbody>"
                            + "  <tr>"
                            + "  <td bgcolor= '#155b7f' nowrap= '' align= 'center' style= 'padding-bottom:10px'><table cellpadding= '0' cellspacing= '0' border= '0' align= 'left'>"
                            + "  <tbody>"
                            + "  <tr>"
                           + "  <td align= 'left' style= 'padding:5px 3px 5px 20px;height:30px' ><a href= '#' style= 'display:inline-block;outline:none' target= '_blank'><img src='https://d3d71ba2asa5oz.cloudfront.net/53000188/images/645071003-1.jpg' width= '24' height= '24' alt= '' style= 'border:0' class='CToWUd'></a></td>"
                            + "  <td align = 'left' style='padding:5px 3px;height:30px'><a href = '#' style='display:inline-block;outline:none' target='_blank'><img src ='https://d3d71ba2asa5oz.cloudfront.net/53000188/images/645071003-1.jpg' width='24' height='24' alt='' style='border:0' class='CToWUd'></a></td>"
                            + "  <td align = 'left' style='padding:5px 3px;height:30px'><a href = '#' style='display:inline-block;outline:none' target='_blank'><img src ='https://d3d71ba2asa5oz.cloudfront.net/53000188/images/645071003-1.jpg' width='24' height='24' alt='' style='border:0' class='CToWUd'></a></td>"
                            + "  <td align = 'left' style='padding:5px 3px;height:30px'><a href = '#' style='display:inline-block;outline:none' target='_blank'><img src ='https://d3d71ba2asa5oz.cloudfront.net/53000188/images/645071003-1.jpg' width='24' height='24' alt='' style='border:0' class='CToWUd'></a></td>"
                            + "  </tr>"
                            + "  </tbody>"
                            + "  </table>"
                            + "  </td>"
                            + "  </tr>"
                            + "  </tbody>"
                            + "  </table></td>"
                            + " </tr>"
                         + "</table>"
                         + "</ div>";
            }
            catch (Exception ex)
            {

            }
            return Email;
        }

        public static bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool CheckDecimalFormat(String stringValue)
        {
            decimal value;
            try
            {
                if (Decimal.TryParse(stringValue, out value))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        
        #region resizeimage
        public static void ResizePhoto(String orignal_path, String tmpImg_fName, String img_ContentType, string AnotherTempPath, int Height, int Width)
        {
            try
            {
                Image image = Image.FromFile(tmpImg_fName);
                int maxWidth = Width;
                int maxHeight = Height;

                //----Image's original width and height---
                int originalWidth = image.Width;
                int originalHeight = image.Height;


                //---- To preserve the aspect ratio----
                float ratioX = (float)maxWidth / (float)originalWidth;
                float ratioY = (float)maxHeight / (float)originalHeight;
                float ratio = Math.Min(ratioX, ratioY);

                //-----New width and height based on aspect ratio----
                int newWidth = (int)(originalWidth * ratio);
                int newHeight = (int)(originalHeight * ratio);

                //---Convert other formats (including CMYK) to RGB.---
                Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);

                //---Fill the background color---
                Color clearColor = new Color();
                try
                {
                    clearColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                }
                catch
                {
                    clearColor = Color.White;
                }

                //---Draws the image in the specified size with quality mode set to HighQuality---
                Graphics graphics = Graphics.FromImage(newImage);
                graphics.Clear(clearColor);
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
                FileInfo fi = new FileInfo(tmpImg_fName);
                if (!Directory.Exists(AnotherTempPath))
                {
                    Directory.CreateDirectory(AnotherTempPath);
                }
                string temp_img_path = AnotherTempPath + fi.Name;
                if (File.Exists(temp_img_path))
                {
                    File.Delete(temp_img_path);
                }
                Bitmap newBitmap = new Bitmap(newImage);
                newImage.Dispose();
                newImage = null;
                newBitmap.Save(temp_img_path);

                image.Dispose();
                graphics.Dispose();

                System.Drawing.Image origPhoto = System.Drawing.Image.FromFile(temp_img_path, true);

                int resizedWidth = Width;
                int resizedHeight = Height;

                int sourceWidth = origPhoto.Width;
                int sourceHeight = origPhoto.Height;
                int destX = 0;
                int destY = 0;
                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;

                nPercentW = ((float)resizedWidth / (float)sourceWidth);
                nPercentH = ((float)resizedHeight / (float)sourceHeight);

                int destWidth = origPhoto.Width;
                int destHeight = origPhoto.Height;
                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                    destX = System.Convert.ToInt16((resizedWidth -
                                  (sourceWidth * nPercent)) / 2);
                }
                else
                {
                    nPercent = nPercentW;
                    destY = System.Convert.ToInt16((resizedHeight -
                                  (sourceHeight * nPercent)) / 2);
                }

                if (sourceWidth < Width && sourceHeight < Height)
                {
                    destX = (int)(((resizedWidth) - destWidth) / 2);
                    destY = (int)(((resizedHeight) - destHeight) / 2);
                }
                else
                {
                    destWidth = (int)(sourceWidth * nPercent);
                    destHeight = (int)(sourceHeight * nPercent);
                }

                origPhoto.Dispose();
                SavePhoto(resizedWidth, resizedHeight, destWidth - 2, destHeight - 2, destX + 1, destY + 1, orignal_path, temp_img_path, img_ContentType);

            }
            catch (Exception ex)
            {

            }
        }

        private static void SavePhoto(int resizedWidth, int resizedHeight, int destWidth, int destHeight, int destX, int destY, String orignal_path, string Temp_img_path, String img_ContentType)
        {
            System.Drawing.Image origPhoto = System.Drawing.Image.FromFile(Temp_img_path, true);

            Bitmap resizedPhoto = new Bitmap(resizedWidth, resizedHeight, PixelFormat.Format24bppRgb);
            resizedPhoto.SetResolution(origPhoto.HorizontalResolution, origPhoto.VerticalResolution);
            Graphics grPhoto = Graphics.FromImage(resizedPhoto);
            Color clearColor = new Color();

            String fillColor = "#FFFFFF";
            int resizedQuality = 100;

            try
            {
                clearColor = System.Drawing.ColorTranslator.FromHtml(fillColor);
            }
            catch
            {
                clearColor = Color.White;
            }
            grPhoto.Clear(clearColor);

            // Encoder parameter for image quality 
            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, resizedQuality);

            if (img_ContentType == "image/gif" || img_ContentType == ".jpg" || img_ContentType == ".png" || img_ContentType == ".jpeg")
                img_ContentType = "image/jpeg";

            // Image codec 
            ImageCodecInfo imgCodec = GetEncoderInfo(img_ContentType);

            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;

            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(origPhoto,
            new Rectangle(destX, destY, destWidth, destHeight),
            new Rectangle(0, 0, origPhoto.Width, origPhoto.Height),
            GraphicsUnit.Pixel);
            try
            {
                if (File.Exists(orignal_path))
                {
                    try
                    {
                        File.Delete(orignal_path);
                    }
                    catch (Exception)
                    {

                    }
                }
                resizedPhoto.Save(orignal_path, imgCodec, encoderParams);
            }
            catch (Exception ex)
            {

                //throw new Exception("You do not have proper permissions set.  You must ensure that the ASPNET and NETWORK SERVICE users have write/modify permissions on the images directory and it's sub-directories.");
            }
            finally
            {
                grPhoto.Dispose();
                origPhoto.Dispose();
                resizedPhoto.Dispose();
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();

                if (File.Exists(Temp_img_path))
                {
                    try
                    {
                        File.Delete(Temp_img_path);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
        }
        private static ImageCodecInfo GetEncoderInfo(string resizeMimeType)
        {
            // Get image codecs for all image formats 
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec 
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == resizeMimeType)
                    return codecs[i];
            return null;
        }
        #endregion resizeimage

        public static void RemoveImages(string ImagePath, string ImageName)
        {
            try
            {
                if (Directory.GetFiles(ImagePath, ImageName + "_" + "*").Any())
                {
                    foreach (string file in Directory.EnumerateFiles(ImagePath, ImageName + "_" + "*"))
                    {
                        File.Delete(file);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    if (prop.PropertyType == typeof(DateTime) && ((DateTime)prop.GetValue(item)) == DateTime.MinValue)
                    {
                        row[prop.Name] = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                    }
                    else
                    {
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                    }
                }
                table.Rows.Add(row);
            }
            return table;
        }


    }
}
