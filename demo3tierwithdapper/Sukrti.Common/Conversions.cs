﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sukrti.Common
{
    public static class Conversion
    {
        public static int GetInteger(object o)
        {
            int val = 0;
            if (o != null && !string.IsNullOrEmpty(o.ToString()))
            {
                int.TryParse(o.ToString(), out val);
            }
            return val;
        }

        public static bool Boolean(object o)
        {
            bool val = false;
            if (o != null && !string.IsNullOrEmpty(o.ToString()))
            {
                bool.TryParse(o.ToString(), out val);
                //val = true;
            }
            return val;
        }

        public static string GetString(object o)
        {
            string val = "";
            if (o != null && !string.IsNullOrEmpty(o.ToString()) && o.ToString() != "null")
            {
                val = o.ToString();
            }
            return val;
        }

        public static decimal Decimal(object o)
        {
            decimal d = 0;
            if (o != null && !string.IsNullOrEmpty(o.ToString()))
            {
                decimal.TryParse(o.ToString(), out d);
                d = Convert.ToDecimal(string.Format("{0:0.00}", d));
            }
            return d;
        }

        public static double Double(object o)
        {
            double d = 0;
            if (o != null && !string.IsNullOrEmpty(o.ToString()))
            {
                double.TryParse(o.ToString(), out d);
                d = Math.Round(d, 2);
            }
            return d;
        }

        public static DateTime Datetime(object o)
        {
            DateTime d = new DateTime();
            if (o != null && !string.IsNullOrEmpty(o.ToString()))
            {
                DateTime.TryParse(o.ToString(), out d);
            }
            return d;
        }

        public static DateTime GetDateFromString(string date)
        {
            DateTime WeekEnd;
            System.Globalization.CultureInfo culture;
            culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            System.Globalization.DateTimeStyles styles;
            styles = System.Globalization.DateTimeStyles.None;
            DateTime.TryParse(date, culture, styles, out WeekEnd);
            return WeekEnd;
        }

        public static DateTime ConvertDateFormat(string In_Time)
        {
            DateTime New_Shift_outTime = DateTime.Now;
            DateTime dt = new DateTime();
            string Dateformat = New_Shift_outTime + " " + In_Time;
            dt = Convert.ToDateTime(Dateformat);
            return dt;
        }
    }
}
