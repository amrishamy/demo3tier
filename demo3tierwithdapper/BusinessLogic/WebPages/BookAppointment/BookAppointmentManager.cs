﻿using BusinessObjects.WebPages.BookAppointment;
using DataRepository.WebPages.BookAppointment;
using SqlDapper;
using System;
using EncryptHelper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.WebPages.BookAppointment
{
    public class BookAppointmentManager
    {
        BookAppointmentRepository _objBookAppointmentRepository = new BookAppointmentRepository(BaseDapper.CreateInstance());
        public ResultObject SaveOrdersAndAppointement(BookAppointmentObject _obj)
        {
            ResultObject _returnObj = new ResultObject();
           
            try
            {
                _returnObj = _objBookAppointmentRepository.SaveOrdersAndAppointement(_obj);
                return _returnObj;
               
            }
            catch (Exception ex)
            {
                _returnObj.Message = ex.Message.ToString();
                _returnObj.Status = 0;
            }
            return _returnObj;
        }
    }
}
